require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, name: 'test', taxonomy: taxonomy) }

  before do
    get :show, params: { id: taxon.id }
  end

  it "リクエストが成功する" do
    expect(response.status).to eq(200)
  end

  it "Show Viewがレンダリングされている" do
    expect(response).to render_template :show
  end

  it "モデルオブジェクトが読み込まれている" do
    expect(assigns(:taxonomies).first).to eq taxonomy
    expect(assigns(:category)).to eq taxon
  end
end
