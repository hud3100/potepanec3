require 'rails_helper'

RSpec.feature "Product", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it '商品詳細ページからトップページに移動する' do
    expect(page).to have_link('Home', href: potepan_index_path)
  end

  it '「一覧ページに戻る」をクリックするとカテゴリー一覧へ戻る' do
    click_on '一覧ページへ戻る'
    expect(page).to have_content(taxon.name)
  end

  it '商品詳細ページからロゴをクリックするとトップページに移動する' do
    link = find('.navbar-brand').click
    expect(link[:href]).to eq potepan_index_path
  end

  it '商品の名前、説明、価格が表示されていること' do
    expect(page).to have_content(product.name)
    expect(page).to have_content(product.description)
    expect(page).to have_content(product.display_price)
  end
end
