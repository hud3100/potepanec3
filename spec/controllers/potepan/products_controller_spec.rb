require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let!(:product) { create(:product) }

  before do
    get :show, params: { id: product.id }
  end

  it "リクエストが成功する" do
    expect(response.status).to eq(200)
  end

  it "Show Viewがレンダリングされている" do
    expect(response).to render_template :show
  end

  it "モデルオブジェクトが読み込まれている" do
    expect(assigns(:product)).to eq product
  end
end
