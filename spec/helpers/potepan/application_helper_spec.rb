require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  it "引数がnilのときベースタイトルを返す" do
    expect(full_title(nil)).to eq "BIGBAG Store"
  end

  it "ベースタイトルのみを返す" do
    expect(full_title('')).to eq "BIGBAG Store"
  end

  it "ページごとの完全なタイトルを返す" do
    expect(full_title('test')).to eq "test - BIGBAG Store"
  end
end
