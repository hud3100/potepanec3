require 'rails_helper'

RSpec.feature "category", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, name: "Bag", taxonomy: taxonomy) }
  let(:othertaxon) { create(:taxon, name: "Clothes", taxonomy: taxonomy) }
  let!(:product1) { create(:product, name: "Tote", price: 10, taxons: [taxon]) }
  let!(:product2) { create(:product, name: "Hand bag", price: 15, taxons: [taxon]) }
  let!(:product3) { create(:product, name: "T-shirt", price: 100, taxons: [othertaxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it 'カテゴリーが表示される' do
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
  end

  it 'カテゴリーに属した商品の名前、価格が表示される' do
    expect(page).to have_content product1.name
    expect(page).to have_content product1.price
  end

  it 'カテゴリーに属した商品の数だけ表示される' do
    products = taxon.all_products
    expect(products.count).to eq(2)
  end

  it 'カテゴリー外の商品は表示されない' do
    expect(page).not_to have_content product3.name
    expect(page).not_to have_content product3.price
  end

  it '商品名をクリックすると商品個別ページへ飛ぶ' do
    click_on product1.name
    expect(page).to have_content product1.name
  end

  it '商品の価格をクリックすると商品個別ページへ飛ぶ' do
    click_on product1.price
    expect(page).to have_content product1.name
  end
end
